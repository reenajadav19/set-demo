# Software Engineer in Test Demo
This is our onsite task for potential new software engineers in test. We expect this task to take less than a hour of work

Please fork this repo on Gitlab and make a starting commit with the message 'start'.
Commit your finished code and push to your forked repo when you are finished. Thanks 😄

## TODO

Add a [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/) file such that your end to end test runs in the CI pipeline for every new push
The pipeline should pass or fail based on the status of the end to end test


## Notes
* Here is a quick [get started guide](https://docs.gitlab.com/ee/ci/quick_start/) for writen a .gitlab-ci.yml file 
* You can/should use the free [shared runners](https://docs.gitlab.com/ee/user/gitlab_com/#shared-runners) that are enabled by default on every Gitlab.com project
* If you run into any technical difficulties contact henry@snaptravel.com
